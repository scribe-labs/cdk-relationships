using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scribe.Core.ConnectorApi.Metadata;

namespace TestRelationshipExtensions
{
    [TestClass]
    public class RelationshipsMetadataTests
    {


        [TestMethod]
        public void TestCreatingAParentRel()
        {
            var abcObject = AbcObject; // assume a valid ObjectDef
            var xyzObject = XyzObject; // assume a valid ObjectDef

            var parentRel = abcObject.DescribeManyToOneRelationship(xyzObject, "a", "p", "ParentC ");

            Assert.AreEqual(RelationshipType.Parent, parentRel.RelationshipType);
            Assert.AreEqual(abcObject.FullName, parentRel.ThisObjectDefinitionFullName);
            Assert.AreEqual(xyzObject.FullName, parentRel.RelatedObjectDefinitionFullName);
            Assert.AreEqual("a", parentRel.ThisProperties);
            Assert.AreEqual("p", parentRel.RelatedProperties);
        }

        [TestMethod]
        public void TestCreatingAChildRel()
        {
            var abcObject = AbcObject;
            var xyzObject = XyzObject;

            var childRel = abcObject.DescribeOneToManyRelationship(xyzObject, "a", "p", "ChildrenC");

            Assert.AreEqual(RelationshipType.Child, childRel.RelationshipType);
            Assert.AreEqual(abcObject.FullName, childRel.ThisObjectDefinitionFullName);
            Assert.AreEqual(xyzObject.FullName, childRel.RelatedObjectDefinitionFullName);
            Assert.AreEqual("a", childRel.ThisProperties);
            Assert.AreEqual("p", childRel.RelatedProperties);
        }

        private static ObjectDefinition _xyzObject;
        private static ObjectDefinition XyzObject
        {
            get
            {
                if (_xyzObject == null)
                {
                    var propertyDefinitions = new List<IPropertyDefinition>();
                    var p = new PropertyDefinition { FullName = "p", Name = "p", PropertyType = typeof(string).FullName };

                    propertyDefinitions.Add(p);

                    var q = new PropertyDefinition { FullName = "q", Name = "q", PropertyType = typeof(string).FullName };

                    propertyDefinitions.Add(q);

                    var xyzObject = new ObjectDefinition { FullName = "XYZ", PropertyDefinitions = propertyDefinitions };
                    _xyzObject = xyzObject;
                }

                return _xyzObject;

            }
        }


        private static ObjectDefinition _abcObject;
        private static ObjectDefinition AbcObject
        {
            get
            {
                if (_abcObject == null)
                {
                    var propertyDefinitions = new List<IPropertyDefinition>();
                    var a = new PropertyDefinition {FullName = "a", Name = "a", PropertyType = typeof (int).FullName};

                    propertyDefinitions.Add(a);

                    var b = new PropertyDefinition {FullName = "b", Name = "b", PropertyType = typeof (string).FullName};

                    propertyDefinitions.Add(b);
                    var abcObject = new ObjectDefinition {FullName = "ABC", PropertyDefinitions = propertyDefinitions};
                    _abcObject = abcObject;
                }

                return _abcObject;
            }
        }
    }
}