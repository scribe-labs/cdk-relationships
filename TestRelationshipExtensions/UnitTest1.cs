﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Relationships;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Metadata;

namespace TestRelationshipExtensions
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestObjectDefinitionIsDefined()
        {
            var de = new DataEntity();
            de.AddParent("Fake", de);

            Assert.IsNotNull(de.Children);
            Assert.IsTrue(de.Children["Fake"].Count == 1);
        }


        [TestMethod]
        public void TestSimplest()
        {
            var de = new DataEntity("A");
            de.AddParent("Fake", de);

            Assert.IsNotNull(de.Children);
            Assert.IsTrue(de.Children["Fake"].Count == 1);
        }

        [TestMethod]
        public void TestSimplestChildren()
        {
            var de = new DataEntity("A");
            de.AddChildren("Fake", new [] { de });

            Assert.IsNotNull(de.Children);
            Assert.IsTrue(de.Children["Fake"].Count == 1);
        }

        [TestMethod]
        public void TestSimplestChildren2()
        {
            var de = new DataEntity("A");
            de.AddChildren("Fake", new[] { de, de });

            Assert.IsNotNull(de.Children);
            Assert.AreEqual(2, de.Children["Fake"].Count);
        }

        [TestMethod]
        public void TestSimplestChildrenWithRelDef()
        {
            var de = new DataEntity("A");
            IRelationshipDefinition rel = new RelationshipDefinition();
            rel.Name = "Fake";
            rel.FullName = "Fake";
            rel.ThisObjectDefinitionFullName = "A";
            rel.RelatedObjectDefinitionFullName = "A";
            rel.RelationshipType = RelationshipType.Child;



            de.AddChildren(rel, new[] { de, de });

            Assert.IsNotNull(de.Children);
            Assert.AreEqual(2, de.Children["Fake"].Count);
        }

        [TestMethod]
        public void TestOtherChildrenWithRelDef()
        {
            var de = new DataEntity("A");
            var cde1 = new DataEntity("B");
            var cde2 = new DataEntity("B");

            IRelationshipDefinition rel = new RelationshipDefinition();
            rel.Name = "Fake";
            rel.FullName = "Fake";
            rel.ThisObjectDefinitionFullName = "A";
            rel.RelatedObjectDefinitionFullName = "B";
            rel.RelationshipType = RelationshipType.Child;

            de.AddChildren(rel, new[] { cde1, cde2 });

            Assert.IsNotNull(de.Children);
            Assert.AreEqual(2, de.Children["Fake"].Count);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Each entity in the collection of children must be of type C, but at least one was of type B.")]
        public void TestOtherChildrenWithBadRelDef()
        {
            var de = new DataEntity("A");
            var cde1 = new DataEntity("B");
            var cde2 = new DataEntity("B");

            IRelationshipDefinition rel = new RelationshipDefinition();
            rel.Name = "Fake";
            rel.FullName = "Fake";
            rel.ThisObjectDefinitionFullName = "A";
            rel.RelatedObjectDefinitionFullName = "C";
            rel.RelationshipType = RelationshipType.Child;

            de.AddChildren(rel, new[] { cde1, cde2 });

            Assert.IsNotNull(de.Children);
            Assert.AreEqual(2, de.Children["Fake"].Count);
        }

        [TestMethod]
        public void TestOtherChildrenConverter()
        {
            var de = new DataEntity("A");
            var cde1 = 3;
            var cde2 = 4;

            Func<int, DataEntity> convert = i => new DataEntity("B");
            IRelationshipDefinition rel = new RelationshipDefinition();
            rel.Name = "Fake";
            rel.FullName = "Fake";
            rel.ThisObjectDefinitionFullName = "A";
            rel.RelatedObjectDefinitionFullName = "B";
            rel.RelationshipType = RelationshipType.Child;

            de.AddChildren(rel, new[] { cde1, cde2 }, convert);

            Assert.IsNotNull(de.Children);
            Assert.AreEqual(2, de.Children["Fake"].Count);
        }
    }
}
